#include <sys/mman.h>
#include <unistd.h>

#include <context.hpp>

namespace context {
using detail::ContextStorage;

extern "C" {
void SetupImpl(ContextStorage* storage, void* stack, void (*func)(void*),
               void* info);
void SwitchToImpl(ContextStorage* from, ContextStorage* to);
void ExitToImpl(ContextStorage* to);
}

void Context::Setup(StackView stack, void (*func)(void*), void* info) {
  SetupImpl(&storage_, stack.End(), func, info);
}

void Context::SwitchTo(Context& next) {
  SwitchToImpl(&storage_, &next.storage_);
}

void Context::ExitTo(Context& next) { ExitToImpl(&next.storage_); }

size_t GetPageSize() {
  static size_t page_size = getpagesize();
  return page_size;
}

Stack Stack::AllocatePages(size_t count_pages) {
  size_t count_bytes = GetPageSize() * count_pages;

  void* memory =
      mmap(/*start=*/nullptr, /*length=*/count_bytes,
           /*prot=*/PROT_READ | PROT_WRITE,
           /*flags=*/MAP_PRIVATE | MAP_ANONYMOUS, /*fd=*/-1, /*offset=*/0);

  if (memory == MAP_FAILED) {
    return Stack(nullptr, 0);
  }

  return Stack(memory, count_bytes);
}

void Stack::Release() {
  if (memory_ == nullptr) {
    return;
  }

  munmap(memory_, size_);
  memory_ = nullptr;
  size_ = 0;
}
}  // namespace context

