#pragma once

#include <cstddef>
#include <cstdint>
#include <utility>

namespace context {

namespace detail {
class ContextStorage {
  using Register = void*;
  Register rip, rsp;
  Register rbx, rbp, r12, r13, r14, r15;
};
}  // namespace detail

class Stack {
  friend class StackView;
 public:
  Stack(const Stack&) = delete;
  Stack(Stack&& another) : memory_(another.memory_), size_(another.size_) {
    another.memory_ = nullptr;
    another.size_ = 0;
  }

  Stack& operator=(const Stack&) = delete;
  Stack& operator=(Stack&& another) {
    memory_ = std::exchange(another.memory_, nullptr);
    size_ = std::exchange(another.size_, 0);
    return *this;
  }

  static Stack AllocatePages(size_t count_pages);

  void* Begin() { return memory_; }

  void* End() { return static_cast<uint8_t*>(memory_) + size_; }

  void Release();

  ~Stack() { Release(); }

 private:
  Stack(void* memory, size_t size) : memory_(memory), size_(size) {}
  void* memory_ = nullptr;
  size_t size_ = 0;
};

class StackView {
 public:
  StackView(const Stack& stack) : memory_(stack.memory_), size_(stack.size_) {}

  void* Begin() { return memory_; }

  void* End() { return static_cast<uint8_t*>(memory_) + size_; }

 private:
  void* memory_ = nullptr;
  size_t size_ = 0;
};

struct ITrampoline {
  virtual void Run() = 0;
};

class Context {
  using Function = void (*)(void*);

 public:
  Context() = default;
  Context(Context&&) = default;
  Context& operator=(Context&&) = default;

  Context(const Context&) = delete;
  Context& operator=(const Context&) = delete;

  // Setup run function, before switch to this will be called func(ptr)
  void Setup(StackView stack, void (*func)(void*), void* info);

  void Setup(StackView stack, ITrampoline& trampoline) {
    Setup(stack, RunTrampoline, &trampoline);
  }

  // Switch to next and save current state to this
  void SwitchTo(Context& next);

  // Switch to next
  static void ExitTo(Context& next);

 private:
  static void RunTrampoline(void* trampoline) {
    static_cast<ITrampoline*>(trampoline)->Run();
  }

  detail::ContextStorage storage_;
};
}  // namespace context

