#include <gtest/gtest.h>

#include <context.hpp>
#include <vector>

using context::Context;
using context::ITrampoline;
using context::Stack;

class Tester {
  struct Trampoline : public ITrampoline {
    Trampoline(Tester& tester, size_t index) : tester(tester), index(index) {}
    void Run() override { tester.Test(index); }

    Tester& tester;
    size_t index;
  };

 public:
  Tester(size_t count_contexts)
      : count_contexts_(count_contexts), contexts_(count_contexts_) {
    stacks_.reserve(count_contexts_);
    trampolines_.reserve(count_contexts_);

    const size_t kStackSize = 1;
    for (size_t i = 0; i < count_contexts_; ++i) {
      stacks_.emplace_back(Stack::AllocatePages(kStackSize));
      trampolines_.emplace_back(*this, i);
      contexts_[i].Setup(stacks_[i], trampolines_[i]);
    }
  }

  void Start() {
    main_context_.SwitchTo(contexts_[0]);
    ASSERT_EQ(counter_, count_contexts_);
  }

 private:
  void Foo() { Bar(); }
  void Bar() { throw std::runtime_error("test"); }
  void ExceptionTest() {
    bool caught = false;
    try {
      Foo();
    } catch (...) {
      caught = true;
    }
    ASSERT_TRUE(caught);
  }

  void Test(size_t index) {
    ASSERT_EQ(index, counter_);
    ++counter_;

    ExceptionTest();

    if (index + 1 != count_contexts_) {
      contexts_[index].SwitchTo(contexts_[index + 1]);
    }

    ExceptionTest();

    if (index == 0) {
      Context::ExitTo(main_context_);
    } else {
      Context::ExitTo(contexts_[index - 1]);
    }
  }

 private:
  size_t count_contexts_;

  Context main_context_;

  std::vector<Context> contexts_;
  std::vector<Stack> stacks_;
  std::vector<Trampoline> trampolines_;

  size_t counter_ = 0;
};

TEST(Simple, RecursiveContext) {
  for (size_t i = 2; i < 100; ++i) {
    Tester t(i);
    t.Start();
  }
}
